add_custom_command(OUTPUT ca.key COMMAND certtool ARGS --generate-privkey --outfile ca.key)
add_custom_command(OUTPUT server.key COMMAND certtool ARGS --generate-privkey --outfile server.key)
add_custom_command(OUTPUT client1.key COMMAND certtool ARGS --generate-privkey --outfile client1.key)
add_custom_command(OUTPUT noca_client.key COMMAND certtool ARGS --generate-privkey --outfile noca_client.key)

add_custom_command(OUTPUT ca.pem COMMAND certtool
    ARGS --no-text --generate-self-signed --load-privkey ca.key --outfile ca.pem --template ${CMAKE_CURRENT_SOURCE_DIR}/ca.template
    MAIN_DEPENDENCY ca.key
    DEPENDS ca.template
)

add_custom_command(OUTPUT noca_client.pem COMMAND certtool
    ARGS --no-text --generate-self-signed --load-privkey noca_client.key --outfile noca_client.pem --template ${CMAKE_CURRENT_SOURCE_DIR}/client1.template
    MAIN_DEPENDENCY noca_client.key
    DEPENDS client1.template
)

add_custom_command(OUTPUT server.pem COMMAND certtool
    ARGS --no-text --generate-certificate --load-privkey server.key --outfile server.pem --load-ca-certificate ca.pem
    --load-ca-privkey ca.key  --template ${CMAKE_CURRENT_SOURCE_DIR}/server.template
    MAIN_DEPENDENCY server.key
    DEPENDS ca.pem server.template
)

add_custom_command(OUTPUT client1.pem COMMAND certtool
    ARGS --no-text --generate-certificate --load-privkey client1.key --outfile client1.pem --load-ca-certificate ca.pem
    --load-ca-privkey ca.key  --template ${CMAKE_CURRENT_SOURCE_DIR}/client1.template
    MAIN_DEPENDENCY client1.key
    DEPENDS ca.pem client1.template
)


add_custom_target(certs ALL DEPENDS
    server.pem
    client1.pem
    noca_client.pem
)
