/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "server.h"

#include <time.h>


static void TsAdd(::timespec &ts1, const ::timespec &ts2)
{
    static constexpr int NS_IN_SEC = 1'000'000'000;
    // Add the two timespec variables
    ts1.tv_sec += ts2.tv_sec;
    ts1.tv_nsec += ts2.tv_nsec;
    // Check for nsec overflow
    if (ts1.tv_nsec >= NS_IN_SEC) {
        ts1.tv_sec++;
        ts1.tv_nsec -= NS_IN_SEC;
    }
}


TestServer::TestServer() :
    server_(
            "TestServer",
            "1.2.3",
            +[](timespec *time) {
                return clock_gettime(CLOCK_MONOTONIC, time);
            }),
    task1_(server_.create_task(task1_period, "task1")),
    task2_(server_.create_task(task2_period, "task2"))
{
    task1_.set_signal_readlock(mut_);
    task2_.set_signal_readlock(mut_);
    server_.set_parameter_writelock(mut_);
}

void TestServer::start()
{
    stop();
    running_       = true;
    server_thread_ = std::thread {[this]() {
        constexpr clockid_t used_clock = CLOCK_MONOTONIC;
        unsigned int task_skip         = 0;
        constexpr auto max_task_skip   = TestServer::task2_period.count()
                / TestServer::task1_period.count();
        ::timespec actual_time;
        constexpr auto time_offset = toTimespec(task1_period);
        int counter                = 0;
        while (this->running_) {
            clock_gettime(used_clock, &actual_time);
            setsignal1(counter++);
            this->task1_.update(actual_time);
            if (0 == task_skip) {
                task_skip = max_task_skip;
                this->task2_.update(actual_time);
            }
            task_skip--;
            TsAdd(actual_time, time_offset);
            clock_nanosleep(used_clock, TIMER_ABSTIME, &actual_time, nullptr);
        }
    }};
}

void TestServer::stop()
{
    if (server_thread_.joinable()) {
        running_ = false;
        server_thread_.join();
    }
}

void TestServer::clear()
{
    stop();
    task1_.task_ = nullptr;
    task2_.task_ = nullptr;
    server_      = {};
}

TestServer::~TestServer()
{
    stop();
}

void TestServer::prepare()
{
    server_.prepare();
}
void TestServer::set_config_file_path(const std::string &p)
{
    server_.set_config_file_path(p.c_str());
}

static_assert(
        TestServer::task1_period.count() < TestServer::task2_period.count()
                && TestServer::task2_period.count()
                                % TestServer::task1_period.count()
                        == 0,
        "Bigger period must be a multiple of the smaller one");

const std::chrono::milliseconds TestServer::task1_period;
const std::chrono::milliseconds TestServer::task2_period;
