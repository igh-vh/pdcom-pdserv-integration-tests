/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "../server.h"

#include <chrono>
#include <conf.h>
#include <fstream>
#include <gtest/gtest.h>
#include <memory>
#include <netinet/in.h>
#include <pdcom5/Exception.h>
#include <pdcom5/PosixProcess.h>
#include <pdcom5/Process.h>
#include <pdcom5/SecureProcess.h>
#include <pdcom5/SimpleLoginManager.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <queue>
#include <stdexcept>
#include <sys/socket.h>
#include <sys/timerfd.h>


struct MyLoginManager : PdCom::SimpleLoginManager
{
    enum class NextAction {
        Cancel,
        Throw,
        Alice,
        Bob,
        Unknown,
    };
    std::queue<NextAction> next_action_;
    std::vector<bool> completed_;

    struct LMEx : std::exception
    {};

    using PdCom::SimpleLoginManager::SimpleLoginManager;

    std::string getAuthname() override
    {
        if (next_action_.empty()) {
            ADD_FAILURE() << "No action available";
            throw Cancel();
        }
        auto na = next_action_.front();
        next_action_.pop();
        switch (na) {
            case NextAction::Cancel:
                throw Cancel();
            case NextAction::Alice:
                return "alice@example.com";
            case NextAction::Bob:
                return "bob@example.com";
            case NextAction::Unknown:
                return "eve@example.com";
            default:
                throw LMEx();
        }
    }

    std::string getPassword() override
    {
        if (next_action_.empty()) {
            ADD_FAILURE() << "No action available";
            throw Cancel();
        }
        auto na = next_action_.front();
        next_action_.pop();
        switch (na) {
            case NextAction::Cancel:
                throw Cancel();
            case NextAction::Alice:
                return "Alice!";
            case NextAction::Bob:
                return "Bob!";
            case NextAction::Unknown:
                return "1nsecure";
            default:
                throw LMEx();
        }
    }

    void completed(bool success) override { completed_.push_back(success); }
};

struct MyBroadcast
{
    std::string message_, attribute_, user_;
    std::chrono::nanoseconds time_ns_;
};

template <typename Parent>
class MyProcessBase : public Parent, public PdCom::PosixProcess
{
  public:
    static constexpr auto recv_timeout = std::chrono::seconds {1};
    struct timeout : std::runtime_error
    {
        timeout() : std::runtime_error("read() timeout") {}
    };

    template <typename... Args>
    MyProcessBase(const char *host, unsigned short port, Args &&...args) :
        Parent(std::forward<Args>(args)...), PdCom::PosixProcess(host, port)
    {}

    void readWithTimeout();
    template <typename T>
    void readUntil(T &&predicate, int max_tries = 5)
    {
        for (int i = 0; i < max_tries; ++i) {
            try {
                readWithTimeout();
            }
            catch (timeout const &) {
            }
            if (predicate())
                return;
        }
        throw timeout {};
    }

    void connect()
    {
        readUntil([this]() { return this->connected_; }, 15);
    }

    void connected() override { connected_ = true; }
    void findReply(const PdCom::Variable &var) override
    {
        found_vars_.push_back(var);
    }

    void broadcastReply(
            const std::string &message,
            const std::string &attr,
            std::chrono::nanoseconds time_ns,
            const std::string &user) override
    {
        broadcasts_.push_back({message, attr, user, time_ns});
    }

    bool running_   = true;
    bool connected_ = false;
    std::vector<PdCom::Variable> found_vars_;
    std::vector<MyBroadcast> broadcasts_;
};

template <typename P>
void MyProcessBase<P>::readWithTimeout()
{
    timeval tout {toTimeval(recv_timeout)};
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(PdCom::PosixProcess::fd_, &fds);
    switch (select(
            PdCom::PosixProcess::fd_ + 1, &fds, nullptr, nullptr, &tout)) {
        case 0:
            throw timeout {};
        case 1:
            PdCom::Process::asyncData();
            break;
        default:
            throw std::runtime_error("select() failed");
    }
}


class MyProcess : public MyProcessBase<PdCom::Process>
{
  public:
    MyProcess(const char *host) : MyProcessBase<PdCom::Process>(host, 2345) {}
    int read(char *buf, int count) override
    {
        const int ans = posixRead(buf, count);
        if (ans == 0)
            running_ = false;
        return ans;
    }
    void write(const char *buf, size_t count) override
    {
        posixWriteBuffered(buf, count);
    }


    void flush() override { posixFlush(); }

    static constexpr const char *conf_name = "/pdserv.conf";
};

class SaslProcess : public MyProcess
{
  public:
    using MyProcess::MyProcess;

    static constexpr const char *conf_name = "/pdserv.sasl.conf";
    ::testing::AssertionResult do_login(
            MyLoginManager &lm,
            MyLoginManager::NextAction a1,
            MyLoginManager::NextAction a2,
            bool expected,
            bool call_login)
    {
        lm.completed_.clear();
        lm.next_action_ = {};
        lm.next_action_.push(a1);
        lm.next_action_.push(a2);
        if (call_login and !lm.login())
            return ::testing::AssertionFailure() << "Auth not available";
        readUntil([&lm] { return !lm.completed_.empty(); });
        if (lm.completed_.size() != 1)
            return ::testing::AssertionFailure()
                    << "Recieved " << lm.completed_.size()
                    << " login status updates, expected 1";
        if (lm.completed_.back() != expected)
            return ::testing::AssertionFailure()
                    << "Login was "
                    << (expected ? " not successful"
                                 : " succesful despite wrong password");
        return ::testing::AssertionSuccess();
    }

    ::testing::AssertionResult
    login_ok(MyLoginManager &lm, bool call_login = true)
    {
        return do_login(
                lm, MyLoginManager::NextAction::Alice,
                MyLoginManager::NextAction::Alice, true, call_login);
    }
    ::testing::AssertionResult
    login_wrong_password(MyLoginManager &lm, bool call_login = true)
    {
        return do_login(
                lm, MyLoginManager::NextAction::Alice,
                MyLoginManager::NextAction::Bob, false, call_login);
    }
    ::testing::AssertionResult
    login_unknown_user(MyLoginManager &lm, bool call_login = true)
    {
        return do_login(
                lm, MyLoginManager::NextAction::Unknown,
                MyLoginManager::NextAction::Unknown, false, call_login);
    }
};

static PdCom::SecureProcess::EncryptionDetails
getED(const char *host, const char *client_certs = nullptr)
{
    const auto load_file = [](const std::string &f) {
        std::ifstream if_f(f);
        std::string b;
        getline(if_f, b, '\0');
        return b;
    };
    return {
            load_file(std::string(cert_dir) + "/ca.pem"),
            host,
            client_certs
                    ? load_file(std::string(cert_dir) + client_certs + ".pem")
                    : "",
            client_certs
                    ? load_file(std::string(cert_dir) + client_certs + ".key")
                    : "",
    };
}

class MyTlsProcess : public MyProcessBase<PdCom::SecureProcess>
{
  public:
    MyTlsProcess(const char *host, const char *client_certs = nullptr) :
        MyProcessBase<PdCom::SecureProcess>(
                host,
                4523,
                getED(host, client_certs))
    {
        handshake();
    }
    int secureRead(char *buf, int count) override
    {
        return posixRead(buf, count);
    }
    void secureWrite(const char *buf, size_t count) override
    {
        posixWriteDirect(buf, count);
    }

    static constexpr const char *conf_name = "/pdserv.tls-simple.conf";

    bool isEof() const
    {
        char c;
        return recv(fd_, &c, 1, MSG_PEEK | MSG_DONTWAIT) == 0;
    }
};

class MyTlsClientAuthProcess : public MyTlsProcess
{
  public:
    MyTlsClientAuthProcess(
            const char *host,
            const char *client_certs = "/client1") :
        MyTlsProcess(host, client_certs)
    {}
    static constexpr const char *conf_name = "/pdserv.tls-clientauth.conf";
};

class MySubscriber : public PdCom::Subscriber
{
  public:
    using PdCom::Subscriber::Subscriber;

    void newValues(std::chrono::nanoseconds time_ns) override
    {
        recieved_newvalues_.push_back(time_ns);
    }

    void stateChanged(PdCom::Subscription const &sub) override
    {
        recieved_states_.push_back(sub.getState());
    }

    std::vector<PdCom::Subscription::State> recieved_states_;
    std::vector<std::chrono::nanoseconds> recieved_newvalues_;
};

struct MyDoubleSubscriber : public PdCom::Subscriber
{
    std::vector<std::pair<std::chrono::nanoseconds, double>>
            recieved_newvalues_;
    std::vector<PdCom::Subscription::State> recieved_states_;

    PdCom::Subscription const &sub_;

    MyDoubleSubscriber(
            PdCom::Transmission const &t,
            PdCom::Subscription const &sub) :
        Subscriber(t), sub_(sub)
    {}
    void newValues(std::chrono::nanoseconds time_ns) override
    {
        double d;
        sub_.getValue(d);
        recieved_newvalues_.push_back({time_ns, d});
    }

    void stateChanged(PdCom::Subscription const &sub) override
    {
        if (&sub != &sub_) {
            FAIL() << "Subscription mismatch";
        }
        else {
            recieved_states_.push_back(sub_.getState());
        }
    }
};


template <typename Process>
class PdCom5TestBase : public ::testing::Test
{
  public:
    void SetUp() override
    {
        PdCom::SecureProcess::InitLibrary();
        if constexpr (std::is_same_v<Process, SaslProcess>)
            PdCom::SimpleLoginManager::InitLibrary();
        server_ = std::make_unique<TestServer>();
        server_->set_config_file_path(
                std::string(config_dir) + Process::conf_name);
        ASSERT_NO_THROW(server_->prepare());
        server_->start();
        client_ = std::make_unique<Process>("localhost");
        if constexpr (!std::is_same_v<Process, SaslProcess>)
            client_->connect();
    }
    void TearDown() override
    {
        client_.reset();
        if (server_)
            server_->stop();
        server_.reset();
        if constexpr (std::is_same_v<Process, SaslProcess>)
            PdCom::SimpleLoginManager::FinalizeLibrary();
        PdCom::SecureProcess::FinalizeLibrary();
    }
    static std::unique_ptr<TestServer> server_;
    static std::unique_ptr<Process> client_;

    static void _implCheckBroadcast(
            const std::string &message,
            const char *attribute,
            const char *user = "anonymous",
            int max_td       = 2);
    static void _implParameterEventSubscription();
    static void _implSignalPeriodicSubscription();

    template <typename SecondProcess>
    static void _implParameterUpdateViaSecondConnection();
};

template <typename P>
std::unique_ptr<TestServer> PdCom5TestBase<P>::server_;
template <typename P>
std::unique_ptr<P> PdCom5TestBase<P>::client_;

using PdCom5CppTests    = PdCom5TestBase<MyProcess>;
using PdCom5SaslTests   = PdCom5TestBase<SaslProcess>;
using PdCom5CppTlsTests = PdCom5TestBase<MyTlsProcess>;

using PdCom5CppTlsClientAuthTests = PdCom5TestBase<MyTlsClientAuthProcess>;

TEST(PdCom5CppTestsPlain, PdServNoPrepare)
{
    TestServer server;
    server.set_config_file_path(std::string(config_dir) + "/pdserv.conf");
}

class PortBlocker
{
    const int fd_;

  public:
    PortBlocker(unsigned short port) : fd_(socket(AF_INET, SOCK_STREAM, 0))
    {
        const std::string err_msg =
                "Blocking port " + std::to_string(port) + " failed.";
        if (fd_ != -1) {
            const int optval = 1;
            if (!setsockopt(
                        fd_, SOL_SOCKET, SO_REUSEADDR, &optval,
                        sizeof(optval))) {
                const sockaddr_in addr {
                        AF_INET, htons(port), htonl(INADDR_LOOPBACK)};
                if (!bind(fd_, reinterpret_cast<const sockaddr *>(&addr),
                          sizeof(addr))
                    and !listen(fd_, 5)) {
                    return;
                }
            }
            perror(err_msg.c_str());
            ::close(fd_);
        }
        throw std::runtime_error(err_msg);
    }

    ~PortBlocker() { ::close(fd_); }
};

TEST(PdCom5CppTestsPlain, PortInUse)
{
    const auto run = [](const char *config_file, unsigned short port1,
                        unsigned short port2) {
        PortBlocker const pb1(port1), pb2(port2);
        TestServer server;
        server.set_config_file_path(std::string(config_dir) + config_file);
        server.prepare();
    };

    EXPECT_THROW(
            (run("/pdserv.conf", 2345, 3340)), PdServ::ServBase::prepare_failed)
            << "MSR Port is in use, should throw";
    ASSERT_NO_THROW((run("/pdserv.conf", 3341, 3342)))
            << "MSR Port is now free, should not throw";

    EXPECT_THROW(
            (run("/pdserv.tls-simple.conf", 4523, 3343)),
            PdServ::ServBase::prepare_failed)
            << "Secure MSR Port is in use, should throw";
    ASSERT_NO_THROW((run("/pdserv.tls-simple.conf", 3344, 3345)))
            << "Secure MSR Port is now free, should not throw";


    EXPECT_THROW(
            (run("/pdserv.tls-simple.conf", 4523, 2345)),
            PdServ::ServBase::prepare_failed)
            << "Both Ports are in use, should throw";
    ASSERT_NO_THROW((run("/pdserv.tls-simple.conf", 3347, 3348)))
            << "Both Ports are nowfree, should not throw";
}

TEST(PdCom5CppTestsPlain, MissingConfigFile)
{
    TestServer server;
    server.set_config_file_path("/missing_config_file.yml");
    EXPECT_THROW(server.prepare(), PdServ::ServBase::prepare_failed);
}

TEST_F(PdCom5CppTests, ServerNameVersion)
{
    EXPECT_EQ(client_->name(), "TestServer");
    EXPECT_EQ(client_->version(), "1.2.3");
}


TEST_F(PdCom5CppTlsTests, ServerNameVersion)
{
    EXPECT_EQ(client_->name(), "TestServer");
    EXPECT_EQ(client_->version(), "1.2.3");
}

TEST_F(PdCom5CppTlsClientAuthTests, ServerNameVersion)
{
    EXPECT_EQ(client_->name(), "TestServer");
    EXPECT_EQ(client_->version(), "1.2.3");
}

template <typename P>
void PdCom5TestBase<P>::_implParameterEventSubscription()
{
    client_->find("/dir1/param01");
    client_->readUntil([&c = *client_] { return !c.found_vars_.empty(); });
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const auto var = client_->found_vars_.back();
    ASSERT_FALSE(var.empty()) << "Variable must not be empty";
    EXPECT_EQ(var.getPath(), "/dir1/param01");
    EXPECT_TRUE(var.getSizeInfo().isScalar());
    EXPECT_EQ(var.getTypeInfo().element_size, sizeof(int));
    MySubscriber ms(PdCom::event_mode);
    PdCom::Subscription sub(ms, var);
    client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    });
    if (!ms.recieved_newvalues_.empty()) {
        int i = 909090;
        sub.getValue(i);
        EXPECT_EQ(i, 0) << "Parameter has not been written yet";
        ms.recieved_newvalues_.clear();
    }
    const int test_data = 47811;
    EXPECT_TRUE(var.setValue(test_data)) << "Variable is writeable";
    client_->readUntil([&ms] { return !ms.recieved_newvalues_.empty(); });
    {
        int i = 808080;
        sub.getValue(i);
        EXPECT_EQ(i, test_data)
                << "Parameter update notification arrived correctly";
    }
    EXPECT_EQ(server_->getparam1(), test_data)
            << "Parameter was written correctly";
}


TEST_F(PdCom5CppTests, ParameterEventSubscription)
{
    PdCom5CppTests::_implParameterEventSubscription();
}


TEST_F(PdCom5CppTlsTests, ParameterEventSubscription)
{
    PdCom5CppTlsTests::_implParameterEventSubscription();
}

template <typename P>
void PdCom5TestBase<P>::_implSignalPeriodicSubscription()
{
    client_->find("/dir1/signal1");
    client_->readUntil([&c = *client_] { return !c.found_vars_.empty(); });
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const auto var = client_->found_vars_.back();
    ASSERT_FALSE(var.empty()) << "Variable must not be empty";
    EXPECT_EQ(var.getPath(), "/dir1/signal1");
    EXPECT_TRUE(var.getSizeInfo().isScalar());
    EXPECT_EQ(var.getTypeInfo().element_size, sizeof(double));
    PdCom::Subscription sub1;
    constexpr int decimation = 10;
    constexpr auto period    = TestServer::task1_period * decimation;
    MyDoubleSubscriber subscriber(period, sub1);
    sub1 = {subscriber, var};
    ASSERT_NE(sub1.getState(), PdCom::Subscription::State::Invalid)
            << "Subscription has left invalid state";
    client_->readUntil([&subscriber] {
        return !subscriber.recieved_states_.empty()
                && subscriber.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    });
    client_->readUntil(
            [&subscriber] {
                return subscriber.recieved_newvalues_.size() >= 5;
            },
            10);
    sub1 = {};
    for (unsigned int i = 0; i < subscriber.recieved_newvalues_.size() - 1;
         i += 2) {
        EXPECT_NEAR(
                subscriber.recieved_newvalues_[i].first.count(),
                (subscriber.recieved_newvalues_[i + 1].first - period).count(),
                std::chrono::nanoseconds {TestServer::task1_period}.count() / 2)
                << "Timestamp mismatch";
        EXPECT_EQ(
                subscriber.recieved_newvalues_[i].second,
                subscriber.recieved_newvalues_[i + 1].second - decimation)
                << "Value mismatch";
    }
    SUCCEED() << "Value offset is "
              << subscriber.recieved_newvalues_.front().second;
}

TEST_F(PdCom5CppTests, SignalPeriodicSubscription)
{
    PdCom5CppTests::_implSignalPeriodicSubscription();
}

TEST_F(PdCom5CppTlsTests, SignalPeriodicSubscription)
{
    PdCom5CppTlsTests::_implSignalPeriodicSubscription();
}

template <typename P>
template <typename SecondProcess>
void PdCom5TestBase<P>::_implParameterUpdateViaSecondConnection()
{
    server_->setparam1(4711);
    MySubscriber ms(PdCom::event_mode);
    PdCom::Subscription sub(ms, *client_, "/dir1/param01");
    client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    });
    const auto initial_newvalues = ms.recieved_newvalues_.size();
    SecondProcess client2("localhost");
    client2.connect();
    EXPECT_FALSE(client2.find("/dir1/param01"));
    client2.readUntil([&client2] { return !client2.found_vars_.empty(); });
    ASSERT_EQ(client2.found_vars_.size(), 1) << "One variable has been found";
    const auto var2 = client2.found_vars_.back();
    ASSERT_FALSE(var2.empty()) << "Variable must not be empty";
    const int next_value = 2468;
    ASSERT_TRUE(var2.setValue(next_value))
            << "Setting parameter from second connection";
    client_->readUntil([&ms, initial_newvalues] {
        return ms.recieved_newvalues_.size() > initial_newvalues;
    });
    int read_value = 0;
    sub.getValue(read_value);
    EXPECT_EQ(read_value, next_value)
            << "First connection got correct parameter";
    EXPECT_EQ(server_->getparam1(), next_value)
            << "RT process got correct parameter";
}

TEST_F(PdCom5CppTests, UpdateParameterSecondConnection)
{
    _implParameterUpdateViaSecondConnection<MyProcess>();
}

TEST_F(PdCom5CppTlsTests, UpdateParameterSecondConnection)
{
    _implParameterUpdateViaSecondConnection<MyProcess>();
}

TEST_F(PdCom5CppTlsTests, UpdateParameterSecondConnectionTls)
{
    _implParameterUpdateViaSecondConnection<MyTlsProcess>();
}


template <typename P>
void PdCom5TestBase<P>::_implCheckBroadcast(
        const std::string &message,
        const char *attribute,
        const char *user,
        int max_td)
{
    struct clearer
    {
        std::vector<MyBroadcast> &b_;
        clearer(std::vector<MyBroadcast> &b) : b_(b) {}
        ~clearer() { b_.clear(); }
    } const clearer(client_->broadcasts_);
    client_->broadcast(message, attribute);
    client_->readUntil([&b = client_->broadcasts_] { return b.size() == 1; });
    EXPECT_EQ(client_->broadcasts_.back().message_, message);
    EXPECT_EQ(client_->broadcasts_.back().attribute_, attribute);
    EXPECT_EQ(client_->broadcasts_.back().user_, user);
    ::timespec t;
    ASSERT_EQ((clock_gettime(CLOCK_MONOTONIC, &t)), 0);
    EXPECT_LT(
            std::abs(
                    t.tv_sec
                    - static_cast<time_t>(
                            std::chrono::duration_cast<std::chrono::seconds>(
                                    client_->broadcasts_.back().time_ns_)
                                    .count())),
            max_td)
            << "Time delta less than " << max_td << " seconds";
}

TEST_F(PdCom5CppTests, Broadcast)
{
    _implCheckBroadcast("My'Message\"With&Special<chars>", "text");
    _implCheckBroadcast("My'Message\"With&Special<chars>", "action");
    std::string s {"My Message"};
    s[2] = '\0';
    EXPECT_THROW(_implCheckBroadcast(s, "text"), PdCom::InvalidArgument);
}

TEST_F(PdCom5CppTlsTests, Broadcast)
{
    _implCheckBroadcast("My'Message\"With&Special<chars>", "text");
    _implCheckBroadcast("My'Message\"With&Special<chars>", "action");
    std::string s {"My Message"};
    s[2] = '\0';
    EXPECT_THROW(_implCheckBroadcast(s, "text"), PdCom::InvalidArgument);
}

TEST(PdCom5CppTlsClientAuthReject, RejectSelfSigned)
{
    struct TlsInit
    {
        TlsInit() { PdCom::SecureProcess::InitLibrary(); }
        ~TlsInit() { PdCom::SecureProcess::FinalizeLibrary(); }
    } const tlsinit;

    const auto server_ = std::make_unique<TestServer>();
    server_->set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-clientauth.conf");

    server_->prepare();
    server_->start();
    const auto client_ = std::make_unique<MyTlsClientAuthProcess>(
            "localhost", "/noca_client");
    // gnutls_handshake() returns 0, howewer
    // EXPECT_THROW(client_ = connect(), PdCom::PosixProcess::ReadFailure);
    ASSERT_TRUE(client_);
    client_->readUntil(
            [&client_] { return client_->connected_ or client_->isEof(); });
    EXPECT_TRUE(client_->isEof()) << "Server has closed the socket";
    EXPECT_FALSE(client_->connected_);
}

TEST_F(PdCom5SaslTests, LoginUnknownUser)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    EXPECT_TRUE((client_->login_unknown_user(lm, false)));
    EXPECT_FALSE(client_->connected_) << "Client is still not connected";
}

TEST_F(PdCom5SaslTests, LoginWrongPassword)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    EXPECT_TRUE(client_->login_wrong_password(lm, false));
    EXPECT_FALSE(client_->connected_) << "Client is still not connected";
}

TEST_F(PdCom5SaslTests, CancelLogin)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    lm.next_action_.push(MyLoginManager::NextAction::Cancel);
    client_->readUntil([&lm] { return !lm.completed_.empty(); });
    EXPECT_FALSE(lm.completed_.back()) << "Login was canceled";
    EXPECT_FALSE(client_->connected_) << "Client is still not connected";
    EXPECT_TRUE(client_->login_ok(lm));

    client_.reset();
    EXPECT_THROW(lm.login(), PdCom::ProcessGoneAway)
            << "No Process use after free";
}

TEST_F(PdCom5SaslTests, NoAuthManager)
{
    EXPECT_THROW(client_->connect(), PdCom::LoginRequired);
}

TEST_F(PdCom5SaslTests, ChangeAuthManager)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    ASSERT_TRUE(client_->login_ok(lm, false));
    client_->connect();
    lm.logout();
    {
        MyLoginManager lm2("localhost");
        client_->setAuthManager(&lm2);
        EXPECT_THROW(lm.login(), PdCom::ProcessGoneAway)
                << "Login is no longer possible because AuthManager is "
                   "replaced";

        ASSERT_TRUE(client_->login_ok(lm2));
        client_->reset();
        EXPECT_THROW(lm2.logout(), PdCom::NotConnected) << "No use after free";
    }
}

TEST_F(PdCom5SaslTests, MoveConstructedAuthManager)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    ASSERT_TRUE(client_->login_ok(lm, false));
    client_->connect();
    lm.logout();
    {
        MyLoginManager lm2 {std::move(lm)};
        EXPECT_THROW(lm.login(), PdCom::Exception)
                << "Login is no longer possible because AuthManager is "
                   "moved from";
        EXPECT_THROW(lm.logout(), PdCom::Exception)
                << "Logout is no longer possible because AuthManager is "
                   "moved from";

        client_->setAuthManager(&lm2);
        ASSERT_TRUE(client_->login_ok(lm2));
        lm2.logout();
    }
}

TEST_F(PdCom5SaslTests, MoveAssignedAuthManager)
{
    struct NullProcess : PdCom::Process
    {
        int read(char *, int) override { return -1; }
        void write(const char *, size_t) override {}
        void flush() override {}
        void connected() override {}

        using Process::Process;
    } p2;
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    ASSERT_TRUE(client_->login_ok(lm, false));
    client_->connect();
    lm.logout();
    {
        MyLoginManager lm2 {"localhost"};
        p2.setAuthManager(&lm2);
        lm2 = std::move(lm);

        EXPECT_THROW(lm.login(), PdCom::Exception)
                << "Login is no longer possible because AuthManager is "
                   "moved from";
        EXPECT_THROW(lm.logout(), PdCom::Exception)
                << "Logout is no longer possible because AuthManager is "
                   "moved from";

        ASSERT_TRUE(client_->login_ok(lm2))
                << "AM is now assigned to Process 1";
        lm2.logout();
    }
}


TEST(PdCom5PersistentTests, PersistentParameter)
{
    const char *const parameter = "/dir1/param01";
    const int param1_value      = 4711;
    {
        TestServer server;
        server.set_config_file_path(
                std::string(config_dir) + "/pdserv.persistent.conf");
        ASSERT_NO_THROW(server.prepare());
        server.start();
        MyProcess client("localhost");
        client.connect();
        client.find(parameter);
        client.readUntil([&client] { return !client.found_vars_.empty(); });
        ASSERT_FALSE(client.found_vars_.back().empty()) << "Parameter found";
        ASSERT_TRUE(client.found_vars_.back().setValue(param1_value));
        bool param_updated = false;
        for (int i = 0; i < 100; ++i) {
            if (server.getparam1() == param1_value) {
                param_updated = true;
                break;
            }
            usleep(1000);
        }
        ASSERT_TRUE(param_updated) << "RT Process got parameter update";
    }
    {
        TestServer server;
        server.set_config_file_path(
                std::string(config_dir) + "/pdserv.persistent.conf");
        ASSERT_NO_THROW(server.prepare());
        server.start();
        MyProcess client("localhost");
        client.connect();
        MySubscriber ms(PdCom::poll_mode);
        PdCom::Subscription sub1(ms, client, parameter);
        client.readUntil([&sub1] {
            return sub1.getState() == PdCom::Subscription::State::Active;
        });
        sub1.poll();
        client.readUntil([&ms] { return !ms.recieved_newvalues_.empty(); });
        int v = 111111;
        sub1.getValue(v);
        ASSERT_EQ(v, param1_value) << "Parameter value is persistent";
    }
}
