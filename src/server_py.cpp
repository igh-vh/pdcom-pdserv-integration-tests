/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif

#include "server.h"

#include <Python.h>
#include <pybind11/chrono.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>


namespace py = pybind11;

PYBIND11_MODULE(pditserver, m)
{
    py::class_<TestServer>(m, "TestServer")
            .def(py::init())
            .def_property(
                    "signal1", &TestServer::getsignal1, &TestServer::setsignal1)
            .def_property(
                    "signal2", &TestServer::getsignal2, &TestServer::setsignal2)
            .def("clear", &TestServer::clear)
            .def("start", &TestServer::start)
            .def("stop", &TestServer::stop)
            .def("prepare", &TestServer::prepare)
            .def("set_config_file_path", &TestServer::set_config_file_path)
            .def_property_readonly_static(
                    "task1_period",
                    [](py::object /* self */) {
                        return TestServer::task1_period;
                    })
            .def_property_readonly_static(
                    "task2_period",
                    [](py::object /* self */) {
                        return TestServer::task2_period;
                    })
            //
            ;
}
