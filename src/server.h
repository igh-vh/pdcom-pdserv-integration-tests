/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include <array>
#include <atomic>
#include <chrono>
#include <memory>
#include <mutex>
#include <pdserv.h>
#include <pdservpp.h>
#include <thread>

#define DECLARE_SET_GET(dtype, name) \
  public: \
    dtype get##name() \
    { \
        std::lock_guard<std::mutex> lck(mut_); \
        return name##_.val_; \
    } \
    void set##name(dtype val) \
    { \
        std::lock_guard<std::mutex> lck(mut_); \
        name##_.val_ = val; \
    }

#define DECLARE_SIGNAL(dtype, name, task, decimation, path) \
  private: \
    struct name##wrapper \
    { \
        dtype val_ = {}; \
        name##wrapper(PdServ::Task &t) { t.signal(decimation, path, val_); } \
    } name##_ = {task}; \
\
    DECLARE_SET_GET(dtype, name)

#define DECLARE_PARAMETER(dtype, name, mode, path) \
  private: \
    struct name##wrapper \
    { \
        dtype val_ = {}; \
        name##wrapper(PdServ::ServBase &s) { s.parameter(path, mode, val_); } \
    } name##_ = {server_}; \
\
    DECLARE_SET_GET(dtype, name)


class TestServer
{
    std::mutex mut_;
    PdServ::Server server_;
    PdServ::Task task1_, task2_;
    std::thread server_thread_;
    std::atomic_bool running_;

  public:
    static constexpr auto task1_period = std::chrono::milliseconds {10};
    static constexpr auto task2_period = std::chrono::milliseconds {200};

    using signal2_type = std::array<short, 4>;

    TestServer();
    ~TestServer();

    void start();
    void stop();

    void prepare();
    void set_config_file_path(const std::string &p);
    void clear();


    DECLARE_SIGNAL(double, signal1, task1_, 1, "/dir1/signal1")
    DECLARE_SIGNAL(signal2_type, signal2, task1_, 1, "/dir1/signal2")
    DECLARE_PARAMETER(int, param1, 0666, "/dir1/param01")
};


template <typename T>
constexpr timespec toTimespec(std::chrono::duration<int64_t, T> ms)
{
    const auto secs = std::chrono::duration_cast<std::chrono::seconds>(ms);
    return {secs.count(),
            std::chrono::duration_cast<std::chrono::nanoseconds>(ms - secs)
                    .count()};
}

template <typename T>
constexpr timeval toTimeval(std::chrono::duration<int64_t, T> ms)
{
    const auto secs = std::chrono::duration_cast<std::chrono::seconds>(ms);
    return {secs.count(),
            std::chrono::duration_cast<std::chrono::microseconds>(ms - secs)
                    .count()};
}
