PdCom and PdServ integration tests
==================================

This repository will provide tools to test different versions of [PdCom](https://gitlab.com/etherlab.org/pdcom) and [PdServ](https://gitlab.com/etherlab.org/pdserv) against each other.
